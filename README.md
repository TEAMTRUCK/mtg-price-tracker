Very simple commandline script to scrape and store price info for basically every magic card ever printed. You'll need to create a postgres db prior to running the script, all the other first time run stuff is automated.

Instructions for use: `ruby priceLookup.rb -h` (Be sure to run `bundle install` first.)

Scraping takes a long time, do not panic.