require 'colorize'
require 'nokogiri'
require 'pg'
require 'qruby'
require 'terminal-table'
require 'ascii_charts'
require 'micro-optparse'
require 'optparse'
require 'openssl'
require 'open-uri'
require 'json'
require 'date'

builder = QRuby::Builder.new
QRuby::Builder.escape_character = "'"

options = Parser.new{|p|
	p.banner = "
	--------------------------------------------------------------------------------
	--------------------------------------------------------------------------------
	--------------------------------------------------------------------------------
	-------------------------://-:----------.---------------------------------------
	------------------........-......``````..--..-----------------------------------
	----------------..```                     `....---------------------------------
	--------------.`````                        ``...-------------------------------
	------------.``````..`  `o+os- ```             ``..-----------------------------
	----------.```````.--`  :-` .`   `.``````       ```.----------------------------
	--------..```   `..--`/y+` -y/`   ``.``...``` `  ` ``.------------:y------------
	-------.-``    ```-.` `hs-.-.``      ````-...```````.`---------ss/+o------------
	---:--:-.`     `...``  `/osooo.              `.```````.--------dm/--------------
	:oso+/:-`       `````                             .+o-`--------sNy--------------
	ss/--:-.`           ``                 ``````-`  /s-.``------:smy------//--.----
	o/..`..`                   ``/++/-.```.....:-o..  /:--:h+--/ymNN+-----oN/-------
	--/-`.`           ````      `../ohms:-::/h-+dos`  ../-yMMmdNmmNNNdo---+Ny-------
	-+-.```           `````      ```../sddy/-ss.dh- .ososmMMNddMMNNNmNdy+ymy/+:.----
	+/-.``                ````    ``.-...:sds-.-my.    `oMMNNNmhsymmmMMNNMdmdyss:..-
	o/-.`            `  `--...`....------.`-sd/sh+``.---:oooo+:+s:yNMMMMMMmNNms/---.
	++--`          ` ```.+-:--:-:::::::/:+:../ds:``./ooo-/+ooo+::./mNNNmNMMNdh+:-:--
	-y+-`       `````````//o+/o//soooo//:/:-.`..`  `.....------:///oydNNmNNNh/-.----
	.:ss:       `  ``````-:...```..``             ``...-:.------///o+:ohhhyo/d+----/
	-:++:`        `````````.`````````           ` `````./.------------yyyssso+ssooo:
	-/s::-           `.`.`.```.``   `           ` ``.``.-..---------:sdmh/:-----//--
	+s--/:.       `` ``..` ``````` ` `    `  `  ````````.----------/h+--------------
	hssoo+/`         ``` `.-.`-`.`````    ``   `````````.-.-------------------------
	o.--://-         `   ..``.``-`````   ```  ````` ````----------------------------
	-..---/+.           `.```````````.`` ```   ````````.--/-------------------------
	.-..--///`         ``` `...`````.`.```.`` `````````--::-------------------------
	.:-..--/+/`            `-.``.`.````.````````````.`.:-/--------------------------
	-.//::-:++:`           `.` .```` ```   ```````````:-:/:.------------------------
	/./+//---:+/`          `  ``.````` `` ```` ``````./-///-.-----------------------
	o..-//+:-/+o+.        `     ` `     `   ````````.+-:/---/-----------------------
	o-:oso+/--:::/-`                    `  ````````.+:-/--::------------------------
	+-:/+++///::-://-`         `        `     ``` .+--:--:.-:-----------------------
	/::///+osssyss+-//.                 `        -+:----: .-------------------------
	://yyysoo++d+oso-/:.                `      `:+:.---:  --.-----------------------
	//+ossoo+++h+oo+/-/:.                     ./+/:---:   :-.-----------------------
	/:oooss+/++h+oooo+-/:`                   -+/:+:---`  `:-.-----------------------

	Birb Helps
	"
	p.option(:setname, "The set(s) you're interested in (no need to be exact) => ['Set1+Set2+Set3,...']",
											:default => ''
					)
	p.option(:rarity, "Only show cards of these rarities => ['rare+sealed product+mythic,...']",
										 :default => ''
					)
	p.option(:price, "Ignore any card less than this cost => [0.8]",
										:default => 0
					)
	p.option(:cardname, "The card(s) you're looking for (no need to be exact) => ['Card name 1+card name 2']",
											 :default => ''
					)
	p.option(:graph, "Graph historical price data for each card in your results, at a resolution of [d]ays, [w]eeks, [m]onths, or [y]ears. (DOESN'T WORK YET)",
										:default => 'w'
					)
}.process!

Setlist = options[:setname] ? options[:setname].downcase.split("+") : nil
Rarelist = options[:rarity] ? options[:rarity].downcase.split("+") : nil
Cardlist = options[:cardname] ? options[:cardname].downcase.split("+") : nil
Price = options[:price]

if !File.exists?("credentials.json")
  puts "Credentials not set, give db info\n"
  print "Database name: "
  dbname = gets.chomp
  print "Username: "
  user = gets.chomp
  print "Password: "
  pw = gets.chomp
  credentials = { "db" => dbname, "user" => user, "password" => pw}
  credFile = File.new("credentials.json", "w")
  credFile.write(JSON.pretty_generate(credentials))
  credFile.close
end
Credentials = JSON.parse(File.read("credentials.json"))

if !Dir.exist?("Scrapes")
  Dir.mkdir("Scrapes")
end

begin
	formats = ['standard','modern_one','modern_two','legacy_one','legacy_two']
	url = "https://www.mtggoldfish.com/prices/paper/"
	today = Time.now.strftime("%Y-%m-%d")
	todayPrice = "scrapes/#{today}.json"

	if !File.exist?(todayPrice)
		prices = File.new(todayPrice, "w")
    File.new(".flag", "w")
		out = Hash.new
		puts "getting today's prices..."
		sleep(0.5)

		formats.each{|f|
			puts "getting #{f} prices..."
			page = Nokogiri::HTML(open(url+f, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))

			page.xpath("//div[@class='priceList']//div[@class='priceList-set']").each{ |set|

				if set.text != "\n"

					setname = set.xpath("h3").text.strip
					out[setname] = Hash.new

					#Very ugly string shenanigans for a very ugly scrape.
					rareGroups =
					set.text.split("\n")[5..-1].map{ |a|
            a.empty? ? "@" : a
          }.join("").gsub("@@@@@", "\n").gsub("@@", "|").gsub("@", "\n").gsub("-|", "?\n").split("\n\n")

					rareGroups.each{|r| #turn it into a hash that's comprehensible
						cleaned = r.split("\n").reject{|e| e.empty?}.compact
						rarity = cleaned[0]
						cards = cleaned[1..-1]
						cardHash = Hash[cards.map{|c| c.split("|")}.reject{|e| e.empty?}]
						out[setname][rarity] = cardHash
					}

				end

			}
		}
		puts "done"
		prices.write(JSON.pretty_generate(out))
		prices.close
		sleep(0.5)
  end

  db = PG.connect(:dbname => Credentials["db"] , :user => Credentials["user"], :password => Credentials["password"])
  if File.exists?(".flag")
		puts "mapping data..."
    out = JSON.parse(File.read(todayPrice))
		out.keys.each{ |set|
      #Make sure that any missing tables are added, and update data where appropriate.
			setTableName = db.quote_ident(set)
			setTableExists = db.exec_params("SELECT to_regclass($1)",[setTableName])[0]['to_regclass']

      if !setTableExists
        puts "Database for #{set} not found, building..."
        db.exec("CREATE TABLE #{setTableName} (cardname TEXT UNIQUE, rarity TEXT, price REAL);")
        out[set].keys.each{|rarity|
          out[set][rarity].keys.each{|card|
            cardTableName = db.quote_ident("#{card} [#{set}]")
            db.exec_params("INSERT INTO #{setTableName}(cardname, rarity, price) VALUES($1, $2, 0)", [card, rarity])
          }
        }
      end

			out[set].keys.each{ |rarity|
				out[set][rarity].keys.each{ |card|
					cardTableName = db.quote_ident("#{card} [#{set}]")
					cardTableExists = db.exec_params("SELECT to_regclass($1)",[cardTableName])[0]['to_regclass']
					if !cardTableExists
						db.exec("CREATE TABLE #{cardTableName} (date DATE, price REAL);")
					end
					db.exec(builder.table(cardTableName).where('date', today).delete)
					db.exec_params("INSERT INTO #{cardTableName} VALUES($1, $2)", [ today, out[set][rarity][card].gsub(",","").to_f ])
					db.exec_params("UPDATE #{setTableName} SET price = $1 WHERE cardname = $2", [out[set][rarity][card].gsub(",","").to_f, card])
				}
			}

		}
    File.delete(".flag")
	end

	ValidRarities = ["Mythic","Rare","Uncommon","Common","Basic Land","Special","Sealed Product"]
	Setnames = []
	Rarities = []
	Cardnames = []
	Results = Hash.new


	db.exec("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'").each{ |c|
    #Big lists for user queries.
    if c.values[0].include?("[")
			if !Cardlist.empty?
				Cardlist.each{|e|
					if (c.values[0].match(/.+?(?=\[)/).to_s.downcase.strip.gsub(/[^a-z0-9\s]/i, '')).match(Regexp.new(e.strip.gsub(/[^a-z0-9\s]/i, '')))
						Cardnames.push(c.values[0].match(/.+?(?=\[)/).to_s.strip)
					end
				}
			end
		else
			if !Setlist.empty?
				Setlist.each{|e|
					if (c.values[0].downcase.strip.gsub(/[^a-z0-9\s]/i, '')).match(Regexp.new(e.strip.gsub(/[^a-z0-9\s]/i, '')))
						Setnames.push(c.values[0])
					end
				}
			else
				Setnames.push(c.values[0])
			end
		end
	}
	if !Rarelist.empty?
		Rarelist.each{|e|
			ValidRarities.each{|r|
				if r.downcase.match(Regexp.new(e.strip.gsub(/[^a-z0-9\s]/i, '')))
					Rarities.push(r)
				end
			}
		}
	end
	Setnames.compact!
	Rarities.compact!

	Setnames.each{|set|
    #Search according to the user supplied args.
		query = "SELECT * FROM #{db.quote_ident(set)} WHERE price > $1"
		args = [Price]

		if !Rarities.empty?
			args.push(Rarities[0])
			query += " AND rarity = $#{args.length}"
			if Rarities.length > 1
				Rarities[1..-1].each{|e|
					args.push(e)
					query += " OR rarity = $#{args.length}"
				}
			end
			if !Cardnames.empty?
				args.push Cardnames[0]
				query += " AND (cardname = $#{args.length}"
				if Cardnames.length > 1
					Cardnames[1..-1].each{|card|
						args.push(card)
						query += " OR cardname = $#{args.length}"
					}
				end
				query +=")"
			end
		elsif !Cardnames.empty?
			args.push Cardnames[0]
			query += " AND cardname = $#{args.length}"
			if Cardnames.length > 1
				Cardnames[1..-1].each{|card|
					args.push(card)
					query += " OR cardname = $#{args.length}"
				}
			end
		end

		res = db.exec_params(query, args) #Pretty colors.
		res.each{|e|
			rarity = e.values[1]
			case rarity
			when "Mythic"
				rarity = rarity.red
			when "Rare"
				rarity = rarity.yellow
			when "Uncommon"
				rarity = rarity.white
			when "Special"
			when "Sealed Product"
				rarity = rarity.cyan
			else
				rarity
			end

			Results["#{e.values[0]} [#{set}]"] = rarity
		}
	}

	Results.keys.each{|card| #Pretty graphs
		pts = db.exec(builder.table(db.quote_ident(card)).order_by("date").get_all).map{|res|
			[res['date'],res['price'].to_f]
		}
		puts "\n\n"
		table = Terminal::Table.new(:rows => pts, :headings => ['Date', 'Price'], :title => "#{card.white.bold} | #{Results[card]} | $#{pts[-1][1]}")
		table.align_column(1,:right)
		puts table
		puts "TREND PLOT".underline+"#{AsciiCharts::Cartesian.new(pts).draw}"

	}

rescue PG::Error => e
	puts "Database error\n #{e.message}"
ensure
	db.close
end

#Old manual table formatting stuff
# def lineFormat(name, values, dayUp=0, weekUp=0)
# 	formattedValues = values.map{|s| ('%+10.10s' % s)}
# 	formattedName = ('%-35.35s' % name)
#
# 	if dayUp > 0
# 		formattedValues[1] = formattedValues[1].green
# 		formattedValues[2] = formattedValues[2].green
# 		if weekUp > 0
# 			formattedName = formattedName.cyan.on_black
# 			formattedValues[5] = formattedValues[5].cyan
# 		end
# 	elsif dayUp < 0
# 		formattedValues[1] = formattedValues[1].red
# 		formattedValues[2] = formattedValues[2].red
# 	end
#
# 	if weekUp > 0
# 		formattedValues[3] = formattedValues[3].green
# 		formattedValues[4] = formattedValues[4].green
# 	elsif weekUp < 0
# 		formattedValues[3] = formattedValues[3].red
# 		formattedValues[4] = formattedValues[4].red
# 	end
#
# 	out = " #{formattedName} #{formattedValues.join(" ")}"
# 	return out
# end
#
# def bufferLine(char)
# 	namebuffer = ('%-35.35s' % "").gsub(" ", "-")+char
# 	valuebuffer = ('%+10.10s' % "").gsub(" ", "-")+char
# 	endbuffer = ('%+10.10s' % "").gsub(" ", "-")
# 	return namebuffer + valuebuffer*5 + endbuffer
# end
#
# puts "┌"+bufferLine("┬")+"┐"
# puts lineFormat("Card Name", ["Price", "Today", "Today %", "Weekly", "Weekly %", "Buy"]).bold
# parsedData.keys.each{|key|
# 	if search.match(key.downcase)
# 		info = parsedData[key]
# 		price = info["price"].to_f
# 		buy = price * 0.75 - 1
# 		buy = buy < 0 ? "nuthin" : ('%.2f' % buy)
#
# 		dayC = info["dailyChange"].to_f
# 		dayUp = 0
# 		if dayC < 0
# 			dayUp = -1
# 		elsif dayC > 0
# 			dayUp = 1
# 		end
#
# 		weekC = info["weeklyChange"].to_f
# 		weekUp = 0
# 		if weekC < 0
# 			weekUp = -1
# 		elsif weekC > 0
# 			weekUp = 1
# 		end
#
# 		if price > threshold
# 			puts "├"+bufferLine("┼")+"┤"
# 			puts lineFormat(key, [info.values, buy].flatten, dayUp, weekUp)
# 		end
# 	end
# }
# puts "└"+bufferLine("┴")+"┘"
